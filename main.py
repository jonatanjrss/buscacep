#-*-coding:utf8-*-
try:
    # Sintaxe para python 2
    from urllib import urlencode
except:
    # Sintaxe para python 3
    from urllib.parse import urlencode
import sys
import re
from kivy.app import App
from kivy.lang import Builder
from kivy.config import Config
from kivy.uix.scrollview import ScrollView
from kivy.properties import ObjectProperty, NumericProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.network.urlrequest import UrlRequest
from kivy.metrics import dp, sp
from kivy.core.window import Window

from kivymd.theming import ThemeManager
from kivymd.list import MDList, BaseListItem
from kivymd.label import MDLabel
from kivymd.dialog import MDDialog

import requests
from threading import Thread
from mybs4 import BeautifulSoup
import re


main_widget_kv = '''
#:import NavigationLayout kivymd.navigationdrawer.NavigationLayout
#:import MDTextField kivymd.textfields.MDTextField

MainWindow
<MainWindow>:
    NavigationLayout:
        id: nav_layout
        MDNavigationDrawer:
            NavigationDrawerToolbar:
                title: "Navegação"
            NavigationDrawerIconButton:
                icon: 'checkbox-blank-circle'
                text: "Busca CEP"
                on_release: app.root.ids.scr_mngr.current = 'busca_cep'
            NavigationDrawerIconButton:
                icon: 'checkbox-blank-circle'
                text: "Rastrear Encomenda"
                on_release: app.root.ids.scr_mngr.current = 'rastreamento'
            NavigationDrawerIconButton:
                icon: 'checkbox-blank-circle'
                text: "Sair"
                on_release: app.quit()
        BoxLayout:
            orientation: 'vertical'
            Toolbar:
                id: toolbar
                title: 'Busca CEPS e Encomendas'
                md_bg_color: app.theme_cls.primary_color
                background_palette: 'Primary'
                background_hue: '500'
                left_action_items: [['menu', lambda x: nav_layout.toggle_nav_drawer()]]
                right_action_items: [['information-outline', lambda x: root.show_about_us()]]
            ScreenManager:
                id: scr_mngr
                Screen:
                    name: 'busca_cep'
                    on_enter: root.call_event_leave()
                    MDTextField:    
                        id: text_field
                        hint_text: "Digite seu endereço aqui"
                        helper_text: "Não utilize nº de casa/apto/lote/prédio ou abreviação"
                        helper_text_mode: "on_focus"
                        size_hint: None, None
                        size: 4 * dp(82), dp(48)
                        pos_hint: {'center_x': 0.5, 'center_y': 0.8}
                    MDRaisedButton:
                        text: "Localizar CEP"
                        size_hint: None, None
                        size_hint_x: 0.5
                        height: sp(48)
                        pos_hint: {'center_x': 0.5, 'center_y': 0.4}
                        on_release: app.root.find_cep(root.ids.scr_mngr,root.ids.scrollviewlist,root.ids.text_field)
                Screen:
                    name: 'rastreamento'
                    MDTextField:
                        id: text_field_r
                        hint_text: "Digite seu código de rastreio aqui"
                        size_hint: None, None
                        size: 4 * dp(82), dp(48)
                        pos_hint: {'center_x': 0.5, 'center_y': 0.8}
                    MDRaisedButton:
                        text: "Localizar encomenda"
                        size_hint: None, None
                        size: 4 * sp(48), sp(48)
                        pos_hint: {'center_x': 0.5, 'center_y': 0.4}
                        on_release: app.root.find_order(root.ids.scr_mngr,root.ids.scrollviewlist2,root.ids.text_field_r)
                Screen:
                    name: "find_cep"
                    ScrollView:
                        id: scrollviewlist
                        #do_scroll_x: False

                Screen:
                    name: "found_order"
                    ScrollView:
                        id: scrollviewlist2
                        #do_scroll_x: False
'''


def extract_data_with_regex(data):
    new_data = []
    table = re.findall('<table(.*)</table>', data)
    if not table:
        return None
    else:
        string = table[0].replace('&nbsp;', '')
        addr_cep = re.findall(r'<td width="\d{1,3}">(.*?)</td>', string)
        addr_cep = [i if not i.startswith('<a href=\"javascript:detalhaCep') else re.findall(r';">(.*?)<br><br>', i)[0] for i in addr_cep]
        city_sta = re.findall(r'<td>(.*?)</td>', string)
        if len(addr_cep) == len(city_sta):
            for i in range(0, len(addr_cep), 2):
                addr, cep, city, sta = addr_cep[i], addr_cep[i+1], city_sta[i], city_sta[i+1]
                    
                new_data.append([addr, cep, city, sta])
        return new_data


class FourLineListItem(BaseListItem):
    '''
    A four line list item
    '''
    _txt_top_pad = NumericProperty(sp(16))
    _txt_bot_pad = NumericProperty(sp(15))
    _num_lines = 4

    def __init__(self, **kwargs):
        super(FourLineListItem, self).__init__(**kwargs)
        self.height = sp(104)

class FiveLineListItem(BaseListItem):
    '''
    A five line list item
    '''
    _txt_top_pad = NumericProperty(sp(16))
    _txt_bot_pad = NumericProperty(sp(15))
    _num_lines = 5

    def __init__(self, **kwargs):
        super(FiveLineListItem, self).__init__(**kwargs)
        self.height = sp(130)


class SixLineListItem(BaseListItem):
    '''
    A six line list item
    '''
    _txt_top_pad = NumericProperty(sp(16))
    _txt_bot_pad = NumericProperty(sp(15))
    _num_lines = 6

    def __init__(self, **kwargs):
        super(SixLineListItem, self).__init__(**kwargs)
        self.height = sp(156)

class SevenLineListItem(BaseListItem):
    '''
    A seven line list item
    '''
    _txt_top_pad = NumericProperty(sp(16))
    _txt_bot_pad = NumericProperty(sp(15))
    _num_lines = 7

    def __init__(self, **kwargs):
        super(SevenLineListItem, self).__init__(**kwargs)
        self.height = sp(178)


class EightLineListItem(BaseListItem):
    '''
    A eight line list item
    '''
    _txt_top_pad = NumericProperty(sp(50))
    _txt_bot_pad = NumericProperty(sp(30))
    _num_lines = 8

    def __init__(self, **kwargs):
        super(EightLineListItem, self).__init__(**kwargs)
        self.height = sp(182)


class MainWindow(BoxLayout):
 
    def find_cep(self,scr_mngr, scroll_view_list, text_field):
        self.scroll_view_list = scroll_view_list
        self.scr_mngr = scr_mngr
        url = 'http://www.buscacep.correios.com.br/sistemas/buscacep/resultadoBuscaCepEndereco.cfm'
        params = urlencode({'relaxation': text_field.text, 'semelhante': 'N','tipoCEP': 'ALL'})
        headers ={'Content-type': 'application/x-www-form-urlencoded','Accept':'text/plain'}

        request = UrlRequest(url, on_success=self.ceps_founds, req_body=params,
            req_headers=headers)
        if not text_field.text:
            self.show_dialog(text_field.text)

    def ceps_founds(self, request, result):
        data = extract_data_with_regex(result.decode('latin1'))
        if data:
            self.scroll_view_list.clear_widgets()
            mdlist = MDList()
            self.scroll_view_list.add_widget(mdlist)
            for d in data:
                item = FourLineListItem(text=d[0], secondary_text=d[1] +\
                    '\n'+d[2] + \
                    '\n'+d[3])
                mdlist.add_widget(item)
            self.scr_mngr.current = 'find_cep'
            self.call_event_enter()
        elif not data:
            self.show_dialog()

    def find_order(self,scr_mngr, scroll_view_list, text_field):
        self.scroll_view_list = scroll_view_list
        self.scr_mngr = scr_mngr
        url_base = 'https://www2.correios.com.br/'
        url_rastreamento = 'sistemas/rastreamento/ctrl/ctrlRastreamento.cfm?'
        url = url_base + url_rastreamento
        params = {'acao':'track','btnPesq':'Buscar', 'objetos':text_field.text}
        self.create_thread(url, params)

    def create_thread(self, url, params):
        self.run_thread = Thread(target=self.order_founds,
            args=(url, params))
        self.run_thread.setDaemon(True)
        self.run_thread.start()

    def order_founds(self, url, params):
        resp = requests.post(url, data=params)
        soup = BeautifulSoup(resp.text)
        data = soup.find_all('tr')
        if data:
            self.scroll_view_list.clear_widgets()
            mdlist = MDList()
            self.scroll_view_list.add_widget(mdlist)
            for d in data:
                if len(d) == 4:
                    item = EightLineListItem(text=d[0], secondary_text=d[1] +\
                        '\n'+d[2] + \
                        '\n'+d[3])
                
                elif len(d) == 5:
                    item = FiveLineListItem(text=d[0], secondary_text=d[1] +\
                        '\n'+d[2] + \
                        '\n'+d[3] + \
                        '\n'+d[4])

                elif len(d) == 6:
                    item = SixLineListItem(text=d[0], secondary_text=d[1] +\
                        '\n'+d[2] + \
                        '\n'+d[3] + 
                        '\n'+d[4] + 
                        '\n'+d[5])

                elif len(d) == 7:
                    item = SevenLineListItem(text=d[0], secondary_text=d[1] +\
                        '\n'+d[2] + \
                        '\n'+d[3] + \
                        '\n'+d[4] + \
                        '\n'+d[5] + \
                        '\n'+d[6])

                elif len(d) == 8:
                    item = EightLineListItem(text=d[0], secondary_text=d[1] +\
                        '\n'+d[2] + \
                        '\n'+d[3] + \
                        '\n'+d[4] + \
                        '\n'+d[5] + \
                        '\n'+d[6] + \
                        '\n'+d[7])

                mdlist.add_widget(item)
            self.scr_mngr.current = 'found_order'
            self.call_event_enter()
        elif not data:
            self.show_dialog()

    def show_dialog(self, textfield=True):
        msg = "DADOS NÃO ENCONTRADOS"
        if not textfield:
            msg = "INFORME O ENDEREÇO"
        content = MDLabel(font_style='Body1',
            theme_text_color='Secondary',
            text=msg,
            size_hint_y=None)
        self.dialog = MDDialog(content=content,
            size_hint_x=0.8, size_hint_y=None,
            height="200sp")
        self.dialog.add_action_button("Fechar", action=lambda *x: self.dialog.dismiss())
        self.dialog.open()

    def show_about_us(self):
        msg = """
        Informações sobre o Aplicativo:
        
        Nome: Aplicativo Correios
        Versão: 1.0.2
        Desenvolvedor: Jonatan Rodrigues da Silva
        Email do Desenvolvedor: jonatanjrss@gmail.com
        
        Esse app não possui nenhum vínculo com os Correios."""
        content = MDLabel(font_style='Body1',
            theme_text_color='Secondary',
            text=msg,
            size_hint_y=None,
            height="200sp")
        self.dialog = MDDialog(content=content,
            size_hint_x=0.8, size_hint_y=None,
            height="300sp")
        self.dialog.add_action_button("Fechar", action=lambda *x: self.dialog.dismiss())
        self.dialog.open()

    def call_event_enter(self):
        Window.bind(on_keyboard=self.main_window)

    def call_event_leave(self):
        Window.unbind(on_keyboard=self.main_window)
        
    def main_window(self, window, key, *args):
        if key == 27:
            app = App.get_running_app
            app().root.ids.scr_mngr.current = 'busca_cep'
            return True


class CorreiosApp(App):
    theme_cls = ThemeManager(primary_palette='Yellow')
    def build(self):
        main_widget = Builder.load_string(main_widget_kv)
        return main_widget

    def quit(self):
        self.get_running_app().stop()
        exit()


if __name__ == '__main__':
    CorreiosApp().run()