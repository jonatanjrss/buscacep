"""My Beautiful Soup
DOCSTRING
"""

#__author__ = "Leonard Richardson (leonardr@segfault.org)"
#__version__ = "4.7.1"
#__copyright__ = "Copyright (c) 2004-2019 Leonard Richardson"
# Use of this source code is governed by the MIT license.
#__license__ = "MIT"

import re

__all__ = ['BeautifulSoup']


class BeautifulSoup(object):
    def __init__(self, text):
        cleread_text = '\n'.join(
            [i.strip() for i in text.split('\n') if i.strip()])
        text_without_enter = cleread_text.replace('\n', 'MARKUPn')
        text_without_enter = text_without_enter.replace('&nbsp;', ' ')
        self.text = text_without_enter

    def find_all(self, element):
        data = []
        pattern = '<{tag}>.*?</{tag}>'.format(tag=element)
        founds = re.findall(pattern, self.text)
        if not founds:
            pattern = '<{tag}.*?>(.*?)</{tag}>'.format(tag=element)
        founds = re.findall(pattern, self.text)

        for item in founds:
            search = re.findall('>(.*?)<', item)
            cleared_data = ''.join(search).replace('MARKUPn', '\n').split('\n')
            cleared_data = '\n'.join(cleared_data).replace('\t', '').split('\n')
            cleared_data = [i for i in cleared_data if i]
            data.append(cleared_data)
    
        return data
