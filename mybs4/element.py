import re


class PageElement(object):
    """Contains the navigational information for some part of the page
    (either a tag or a piece of text)"""

    def __init__(self, text):
        cleread_text = '\n'.join(
            [i.strip() for i in resp.text.split('\n') if i.strip()])
        text_without_enter = cleread_text.replace('\n', 'MARKUPn')
        self.text = text_without_enter
        print(self.text)

    def find_all(self, element):
        founds = re.findall('<tr>.*?</tr>', self.text)
        from pprint import pprint as pp
        with open('resultado.txt', 'w') as f:
            f.write(self.text)


class Tag(PageElement):
    """Represents a found HTML tag with its attributes and contents."""
    def __init__(self):
        super(Tag, self).__init__()
        self.arg = arg